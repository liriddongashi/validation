import logo from "./logo.svg";
import "./App.css";
import { Greet } from "./components/greet";
import Welcome from "./components/welcome";
import From from "./components/from";
import Style from "./css/style.css";
import Remember from "./components/remember";
import { useState, useEffect } from "react";
import { render } from "react-router-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import test from "./components/pages/test";
function App() {
  return <From></From>;
}

export default App;
